$(document).ready(function() {
	var baseUrl = $(location).attr("href");
	if($(".url").val()!=''){
		baseUrl=$(".protocol").val()+$(".url").val();
	}
	$(".error_msg").hide();
	$(".reposnse").hide();
	$('.btn-send').click(function() {
	var error=0;
		if($("#currency_1").val()==''){
			$("#currency_1").css('border-color', 'red');
			error=1;
		}else{
			$("#currency_1").css('border-color', '');
			error=0;
		}
		if($("#currency_2").val()==''){
			$("#currency_2").css('border-color', 'red');
			error=1;
		}else{
			$("#currency_2").css('border-color', '');
			error=0;
		}
		if(error==1){
			$(".error_msg").show();
			return false;
		}else{
			$(".error_msg").hide();
			var data_r="?base="+$("#currency_1").val()+"&symbols="+$("#currency_2").val();
			getCORS('https://api.exchangeratesapi.io/latest'+data_r, function(request){
			var response = request.currentTarget.response || request.target.responseText;
			var result = JSON.parse(response);
			var base = result['base'];
			var rate = result['rates'];	
			var date = result['date'];
			$(".message").html("");
			if(request.currentTarget.status =='200'){
				$.ajax( {
					type: "POST",
					url: baseUrl+"/data-handling.php?type=insert&currency1="+$("#currency_1").val()+"&currency2="+$("#currency_2").val()+"&rate="+rate[$("#currency_2").val()]+"&date="+date,
					success: function( data ) {
						$(".message").append(data);
					},
					failure : function(error){
					  alert(error);
					 }
				});
				var currency = "<p>Currency One (Base Currency) : <h3>"+$("#currency_1").val()+"</h3></p><p>Currency Two : <h3>"+$("#currency_2").val()+"</h3></p>";
				var rate_card = "<h3>1 "+$("#currency_1").val()+" = "+rate[$("#currency_2").val()]+" "+ $("#currency_2").val()+"</h3>";
				$(".currency_type").html('');
				$(currency).appendTo('.currency_type');
				$(".result_rate").html('');
				$(rate_card).appendTo('.result_rate');
				$(".reposnse").show();	
			}
			if(request.currentTarget.status =='400'){
				$(".message").append("<p style='color:red'>Kindly check the <strong>Currency Code</strong></p>");
				}
		});
		return true;
		}
		});
	$('.action').click(function () {
		var id = $(this).val();
		$.ajax({
					type: "POST",
					url: baseUrl+"/data-handling.php?type=delete&id="+id,
					success: function( data ) {
						if(data==1){
							$("#id_"+id).remove();
						}
						},
					failure : function(error){
					  alert(error);
					 }
				});
	});

});

function getCORS(url, success) {
    var xhr = new XMLHttpRequest();
    if (!('withCredentials' in xhr)) xhr = new XDomainRequest(); // fix IE8/9
    xhr.open('GET', url);
    xhr.onload = success;
    xhr.send(); 
    return xhr;
}


