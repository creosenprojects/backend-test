<html>
<head>
    <title>Currency Submission form</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='assets/css/custom.css' rel='stylesheet' type='text/css'>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
</head>
<?php
	if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
	$protocol = 'https://';
	}
	else {
	  $protocol = 'http://';
	}
	$dir_name="api";
	
	if($_SERVER['SERVER_PORT']!='80'){
		$url = ($_SERVER['SERVER_NAME']==='localhost')?'localhost:'.$_SERVER['SERVER_PORT'].'/'.$dir_name:$_SERV‌​ER['SERVER_NAME']; 
	}else{
		$url = ($_SERVER['SERVER_NAME']==='localhost')?'localhost/'.$dir_name:$_SERV‌​ER['SERVER_NAME'];
	}
	?>
<body>
	<div class="container form-s">
		<div class="row">
			<input class="url" type="hidden" value="<?php echo $url;?>">
			<input class="protocol" type="hidden" value="<?php echo $protocol;?>">
			<div class="col-md-4">
			<h3 align="left">Currency</h1>
			<br>
				<div class="form-group">
					<label for="currency_1">Currency One</label>
					<input id="currency_1" type="text" name="currency_1" class="form-control" maxlength="3"  placeholder="Please enter your three digit currency Code *" required="required" >
				</div>
				<div class="form-group">
					<label for="currency_2">Currency Two</label>
					<input id="currency_2" type="text" name="currency_2" class="form-control" maxlength="3"  placeholder="Please enter your three digit currency Code *" required="required" >
				</div>
				<div align="left" class="col-md-12">
					<div class="container">
						<div class="row">
							<div class="col-md-12 error_msg">
								<p>Please enter the required fields</p>
							</div>
							<div class="col-md-12">
								<input type="submit" class="btn btn-success btn-send" value="Add it to list">
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-8 reposnse">
				<h3 align="left">Result</h1>
				<div class="result_section">
					<div class="row">
						<div class="col-md-1">
						</div>
						<div class="col-md-5 currency_type">
							
						</div>
						<div class="col-md-6 result_rate">
							
						</div>
					</div>
				</div>					
			</div>
		</div>
		<div class="row ">
			<div class="col-md-12">
				<div class="message"></div>
			</div>
		</div>
		<div class="row ">
			<div class="col-md-3">
				<br><br><a class="btn btn-primary" href="<?php echo $protocol.$url; ?>" role="button">Click here to see the added currency list.</a>
			</div>
		</div>
	</div>
</body>
<script src="assets/js/main.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
</html>