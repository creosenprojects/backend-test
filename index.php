<html>
<head>
    <title>Currency List</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <link href='assets/css/custom.css' rel='stylesheet' type='text/css'>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
</head>
<?php include("config.php");
	$check="SELECT * FROM currency_list";
	$query=$conn->query($check);
	$result=mysqli_fetch_all($query);
	if (isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
	$protocol = 'https://';
	}
	else {
	  $protocol = 'http://';
	}
	$url =$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
	?>
<body>
	<div class="container">
		<div class="row">
		<h3 class="text-center">Currency List</h3>
			<input class="url" type="hidden" value="<?php echo $url;?>">
			<input class="protocol" type="hidden" value="<?php echo $protocol;?>">
			<div class="table-responsive">          
			  <table class="table table-striped table-dark text-center">
				<thead>
				  <tr>
					<th>Currency One</th>
					<th>Currency Two</th>
					<th>Rate</th>
					<th>Date</th>
					<th>Delete</th>
				  </tr>
				</thead>
				<tbody>
					<?php foreach($result as $value){ ?>
					<tr id="id_<?php echo $value[0];?>">
						<td><?php echo $value[1]; ?></td>
						<td><?php echo $value[2]; ?></td>
						<td><?php echo $value[3]; ?></td>
						<td><?php echo $value[4]; ?></td>
						<td>
							<button class="action btn btn-info btn-sm" value="<?php echo $value[0];?>">
								
								  <span class="glyphicon glyphicon-minus"></span>
								
							</button>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			  </table>
			</div>
		</div>
		<div class="row ">
			<div class="col-md-12">
				<div class="message"></div>
			</div>
		</div>
		<div class="row ">
			<div class="col-md-3">
				<a class="btn btn-primary" href="<?php echo $protocol.$url.'/add.php'; ?>" role="button">Click here to add currency</a>
			</div>
		</div>
	</div>
</body>
<script src="assets/js/main.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
</html>