Db Config 
---------

There is file name called config.php. Please update mysql related details on that file.


index.php
----------------

In this file, I am showing the added currency details as table.


Data-Handling.php:
--------------

All the MySql related operations are handled in this file. 


list.php
--------

This file used for compare the currency rate with one another. The currency one field is taken as "Base currency". 

After fill the currency details have to trigger the button. Once it triggered the API call will run to fetch the rate detail. The result will show in the right side of this page.


Used technologies: PHP,MySql,jQuery,Ajax  


Mysql queries 
-------------
To create database - CREATE DATABASE currency;

To add tables and columns - CREATE TABLE currency_list (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,currency_one text ,currency_two text ,rate text,date text);


Note: The database file is attached as currency.sql.zip